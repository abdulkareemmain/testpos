<li class="nav-item">
    <a href="{{ route('users.index') }}" class="nav-link @if(url()->current()==route('users.index')) active @endif">
        <i class="nav-icon fas fa-user"></i>
        <p>Users</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('customers.index') }}" class="nav-link @if(url()->current()==route('customers.index')) active @endif">
        <i class="nav-icon fas fa-user"></i>
        <p>Customers</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('products.index') }}" class="nav-link @if(url()->current()==route('products.index')) active @endif">
        <i class="nav-icon fas fa-user"></i>
        <p>Products</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('sales.index') }}" class="nav-link @if(url()->current()==route('sales.index')) active @endif">
        <i class="nav-icon fas fa-user"></i>
        <p>Sales</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('reports.index') }}" class="nav-link @if(url()->current()==route('reports.index')) active @endif">
        <i class="nav-icon fas fa-user"></i>
        <p>Reports</p>
    </a>
</li>
