@extends('layouts.app')
@section('content')
    <link href="{{ asset('css/sales.css') }}" rel="stylesheet">


    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="heading col-sm-6">
                    <h1>products</h1>
                </div>
                <div class="offset-sm-4  col-sm-2">
                    <h1 class="float-sm-right">
                        {{-- <span style="background-image: linear-gradient(121deg,black  1%, white 300%);
                            color: white;" class="badge badge-pill">{{ $products->total() }}</span> --}}

                    </h1>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <form action="{{ route('sales.store') }}" method="POST">
        <div class="container-fluid">
            <div class="col-md-6 mb-3">
                <label for="">Select Customer</label>
                <select name="customer_id" id="" class="form-control select2">
                    @foreach ($customers as $customer)
                        <option value="{{ $customer->id }}">{{ $customer->name }} Phone: {{ $customer->phone }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        @foreach ($products as $item)

                            <div class="col-md-4">
                                <div class="single-publication">
                                    <figure>
                                        <a href="#">
                                            <img style="width: 100px; height:100px;"
                                                src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png"
                                                alt="Publication Image">
                                        </a>

                                    </figure>

                                    <div class="publication-content">
                                        {{-- <span class="category">Book</span> --}}
                                        <h3><a href="#">{{ $item->name }}</a></h3>
                                        <p>{{ $item->description }}</p>

                                        <h4 class="price">${{ $item->retail_price }} </h4>
                                    </div>

                                    <div class="add-to-cart">
                                        <a href="#" class="default-btn"
                                            onclick="addToCart('{{ $item->id }}','{{ $item->name }}','{{ $item->retail_price }}','1')">Add
                                            to Cart</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Sales</h3>

                            {{-- <div class="card-tools">
                            <ul class="pagination pagination-sm float-right">
                                <li class="page-item"><a class="page-link" href="#">«</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">»</a></li>
                            </ul>
                        </div> --}}
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            @csrf
                            <table id="pos_table" class="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th style="width: 10px">#</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Qty</th>
                                        <th>Total</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody id="pos_tbody">

                                </tbody>
                                <tfoot>
                                    <th>Total</th>
                                    <th id="total"></th>
                                    <th class="text-right"><button class="form-control">Checkout</button></th>
                                </tfoot>
                            </table>
    </form>
    </div>
    <!-- /.card-body -->
    </div>
    <!-- /.card -->
    </div>
    <!-- /.col -->
    </div>
    </div>
    </div>
    <script>
        const products = [];
        var total = 0;

        $(document).ready(function() {});

        function addToCart(id, name, price, qty) {
            if (products.includes(id)) {
                qty = parseInt($('#qty' + id + '').text()) + 1
                subtotal = qty * parseInt(price)
                $('#qty' + id + '').text((qty).toString())
                $('#hidden_qty' + id + '').val(qty)

                $('#subtotal' + id + '').text((subtotal).toString())
            } else {
                subtotal = parseInt(price) * parseInt(qty);
                $('#pos_tbody').append(
                    `
                    <tr id="row` + id + `">
                        <td><i class="fa fa-plus" onclick="addToCart('`+id+`','`+name+`','`+price+`',`+'1'+`)" aria-hidden="true"></i> <i class="fa fa-minus" aria-hidden="true" onclick=minusItem(` + id + `)></i></td>
                        <td>` + products.length + `</td>
                        <td>` + name + `</td>
                        <td>` + price + `</td>
                        <td>
                        <label id="qty` + id + `">` + qty + `</label>
                        <input type="hidden" id="hidden_qty` + id + `" class="form-control" name="quantity[]" value="` + qty + `">
                        <input type="hidden" class="form-control" name="products[]" value="` + id + `">
                        </td>
                        <td id="subtotal` + id + `">` + subtotal + `</td>
                        <td><a href="#" onclick=removeItem(` + id + `)>remove</a></td>
                    </tr>
                `
                )
                products.push(id);

            }
            total = total + parseInt(price)
            $('#total').text(total.toString())
        }

        function removeItem(id) {
            const index = products.indexOf(id.toString());
            if (index > -1) {
                products.splice(index, 1);
            }
            sub_total = parseInt($('#subtotal' + id).text())
            total -= sub_total;
            $('#total').text(total.toString())
            var myobj = document.getElementById("row" + id);
            myobj.remove();
        }

        function minusItem(id){
            qty = parseInt($('#qty' + id).text())
            hidden_qty = $('#hidden_qty' + id).val()
            if(hidden_qty==1){
                removeItem(id)
            }
            hidden_qty=hidden_qty-1;
            console.log(hidden_qty);
            $('#qty' + id).text(hidden_qty.toString())
            $('#hidden_qty' + id).val(hidden_qty)
        }
    </script>

@endsection
