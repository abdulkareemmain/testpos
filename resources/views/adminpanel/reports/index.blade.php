@extends('layouts.app')
@section('content')
    <style>
    </style>

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="heading col-sm-6">
                    <h1>Orders</h1>
                </div>
                <div class="offset-sm-4  col-sm-2">
                    <h1 class="float-sm-right"><span style="background-image: linear-gradient(121deg,black  1%, white 300%);
                            color: white;" class="badge badge-pill">{{ $orders->total() }}</span></h1>
                </div>


                <div class="filtersdiv">
                    <style>
                        .filter-control {
                            display: inline;
                        }

                        .filters .select2-container--default .select2-selection--single {
                            width: 220px;
                            background: black
                        }

                    </style>

                    <div class="filters row">
                        <div class="col-md-12">
                            <form action="{{ route('reports.index') }}">
                                @csrf
                                <div class="float-left mx-3 my-3">
                                    <label>From This:</label>
                                    <input type="date" class="form-control filter-control" name="start_date" id="" @if (request()->get('start_date') !== null) value={{ request()->get('start_date') }} @endif>
                                    {{-- <input type="date" class="form-control filter-control"
                                                                            name="start_date" id="" @if (request()->get('start_date') !== null) value={{ request()->get('start_date') }} @endif> --}}

                                </div>
                                <div class="float-left mx-3 my-3">
                                    <label>To This:</label>
                                    <input type="date" class="form-control filter-control" name="end_date" id="" @if (request()->get('end_date') !== null) value={{ request()->get('end_date') }} @endif>
                                </div>

                                <div class="col-md-3 mx-3 my-3">
                                    <button class="btn btn-primary">Apply Filter</button>
                                </div>
                                {{-- <div class="col-md-3 mx-3 my-3">

                                    <a href="{{ route('reports.index') }}">
                                        <button type="button"
                                            class="btn btn-danger">Cancel</button></a>
                                </div> --}}
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card searcharea">
                    <div class="align-right">
                    </div>
                    <div class="card-header">
                        <br>
                        <div class="card-tools">
                            <div class="input-group input-group-sm">

                            </div>
                        </div>

                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    {{-- <th>#</th> --}}
                                    <th>Order ID</th>
                                    <th>Customer Name</th>
                                    <th>Customer Phone</th>
                                    <th>Order Total</th>
                                    <th>Products Ordered</th>
                                    <th>Order Detail</th>
                                    <th>Created At</th>

                                </tr>
                            </thead>
                            <tbody>
                                <meta name="csrf-token" content="{{ csrf_token() }}" />

                                @forelse ($orders as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->customer->name }}</td>
                                        <td>{{ $item->customer->phone }}</td>
                                        <td>{{ $item->total }}</td>
                                        <td>{{ count($item->products) }}</td>
                                        <td>
                                            <a href="{{ route('reports.show', $item->id) }}" class="float-left"><i
                                                    class="fas fa-eye"></i></a>
                                        </td>

                                        <td>{{ optional($item)->created_at->diffForHumans() }}</td>




                                    </tr>
                                @empty
                                    <p>No Data Found</p>
                                @endforelse
                            </tbody>
                        </table>
                        <div class="align-right paginationstyle">
                            {{ $orders->links() }}
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->


                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    </div>

@endsection
