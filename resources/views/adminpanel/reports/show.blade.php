@extends('layouts.app')
@section('content')
    <style>
    </style>

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="heading col-sm-6">
                    <h1>Order Detail</h1>
                </div>
                <div class="offset-sm-4  col-sm-2">
                    {{-- <h1 class="float-sm-right"><span
                        style="background-image: linear-gradient(121deg,black  1%, white 300%);
                        color: white;"
                            class="badge badge-pill">{{ $orders->total() }}</span></h1> --}}
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card searcharea">
                    <div class="align-right">
                    </div>
                    <div class="card-header">
                        <br>
                        <div class="card-tools">
                            <div class="input-group input-group-sm">


                            </div>
                        </div>

                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    {{-- <th>#</th> --}}
                                    <th>Order ID</th>
                                    <th>Product Name</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Sub Total</th>

                                </tr>
                            </thead>
                            <tbody>
                                <meta name="csrf-token" content="{{ csrf_token() }}" />

                                @forelse ($order->order_details as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->product->name }}</td>
                                        <td>{{ $item->product->retail_price }}</td>
                                        <td>{{ $item->quantity }}</td>
                                        <td>{{ $item->sub_total }}</td>

                                    </tr>
                                @empty
                                    <p>No Data Found</p>
                                @endforelse
                            </tbody>

                            <tfoot>
                                <th>Total</th>
                                <th>{{$item->order->total}}</th>
                            </tfoot>
                        </table>
                        <div class="align-right paginationstyle">
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->


                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    </div>

@endsection
