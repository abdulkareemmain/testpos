<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Order;
use App\OrderDetail;
use App\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::all();
        $customers=Customer::all();
        return view('adminpanel.sales.index',compact('products','customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'customer_id' => 'required',
            'products' => 'required|array',
            'quantity' => 'required|array'
        ]);

        $total=0;
        $order=Order::create(['customer_id'=>$request->customer_id]);
        for($i=0;$i<count($request->products);$i++){
            $product=Product::find($request->products[$i]);
            $sub_total=$product->retail_price*$request->quantity[$i];
            $total+=$sub_total;
            OrderDetail::create([
                'product_id'=>$product->id,
                'customer_id'=>$request->customer_id,
                'quantity'=>$request->quantity[$i],
                'sub_total'=>$sub_total,
                'order_id'=>$order->id
            ]);
        }

        $order->update(['total'=>$total]);

        return redirect()->back()->with(['message'=>'message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
