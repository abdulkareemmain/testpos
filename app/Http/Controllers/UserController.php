<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

        if (!$request->keyword) {
            $users = User::orderBy('created_at', 'desc')->paginate(25);
        } else {
            $users = User::where('name', 'like', '%' . $request->keyword . '%')
                ->orWhere('email', 'like', '%' . $request->keyword . '%')
                ->orWhere('id', 'like', '%' . $request->keyword . '%')
                ->orWhere('phone', 'like', '%' . $request->keyword . '%')
                ->paginate(25)->setPath('');
            $pagination = $users->appends(array(
                'keyword' => $request->keyword
            ));
        }


        return view('adminpanel.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminpanel.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);

        $avatar=null;
        if ($request->hasFile('avatar')) {
            $avatar=$this->uploadFile($request->file('avatar'),'avatars');
        }

        User::create($request->except('password','avatar') + ['password' => Hash::make($request->password),'avatar'=>$avatar]);
        return redirect()->route('users.index')->with(['message'=>'message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(user $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(user $user)
    {
        return view("adminpanel.users.edit", compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, user $user)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);

        $avatar=$user->avatar;
        if ($request->hasFile('avatar')) {
            $avatar=$this->uploadFile($request->file('avatar'),'avatars');
        }
        $user->update($request->except('avatar')+['avatar'=>$avatar]);
        return redirect()->route('users.index')->with(['message'=>'message']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $user)
    {
        $user->delete();
        return redirect()->back();
    }

    public function uploadFile($file,$destination){
        $random = date('YmdHis');
        $completeFileName = $file->getClientOriginalName();
        $fileNameOnly = pathinfo($completeFileName, PATHINFO_FILENAME);
        $name = str_replace(' ', '_', $fileNameOnly) . '-' . time() . $random . '.' . $file->getClientOriginalExtension();
        $file->move($destination, $name);

        return $name;
    }

}
