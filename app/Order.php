<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=[
        'customer_id','total'
    ];

    public function customer(){
        return $this->belongsTo(Customer::class,'customer_id');
    }

    public function products(){
        return $this->hasMany(OrderDetail::class,'order_id');
    }

    public function order_details(){
        return $this->hasMany(OrderDetail::class,'order_id');
    }
}
